#!/usr/bin/bash
# hello-world-wmharrel.sh
# Written by William Harrell (wmharrel)
# Simply prints hello world to the terminal.

# The echo command just takes a string and prints it to stdout.
echo "Hello, World!"

