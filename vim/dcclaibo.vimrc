" ~/.vimrc
" dcclaibo
" Excerpts from my vimrc

" Set foldmethod
set foldmethod=marker

" Set absolute number for current line; relative everywhere else ('hybrid numbering')
set relativenumber
set number
set ruler

" Highlight searches
set hlsearch

" Show search progress
set incsearch

" Clear search highlighting by pressing enter
nnoremap <CR> :nohlsearch<CR>

" Hightlight cursor line
set cursorline

" Maps leader (generic key for mappings, <leader>) key to spacebar
let mapleader = " "

" Allow jk key sequence to stop typing so you don't have to reach to escape
inoremap jk <ESC>

" Visually select text inserted (get inserted)
nnoremap gi `[v`]

" Show Tabs and extra spaces
set list
set listchars=tab:\|\ ,trail:-

" Remove trailing whitespace
nnoremap <LEADER>ts :%s/\(\s\+\)$//g<CR>

" Make j and k not skip wrapped lines, unless given a count
" Can still use `1j` and `1k` to get default behavior
nnoremap <expr> j v:count == 0 ? 'gj' : 'j'
nnoremap <expr> k v:count == 0 ? 'gk' : 'k'
vnoremap <expr> j v:count == 0 ? 'gj' : 'j'
vnoremap <expr> k v:count == 0 ? 'gk' : 'k'
