-- simple-quicksort-wmharrel.hs
-- Written by William Harrell (wmharrel)
-- Simple implementation of the quicksort algorithm in Haskell.

-- This implementation works for any type which is an instance of Ord (meaning it can be compared.)
-- e.g. lists of Ints, Fractionals, Strings
-- Also works on Strings themselves (which are just lists of characters.)
quicksort :: Ord a => [a] -> [a]
quicksort [] = [] -- If list is empty, just return the empty list.
-- (x:xs) will store the first element of the list in x, rest of list in xs.
-- x is a single element. xs is still a list.
quicksort (x:xs) = left ++ x : right -- Otherwise, return left appended to (x appended to right).
  where
    -- left is recursive quicksort of all elements less than x.
    left  = quicksort [x' | x' <- xs, x' <  x] 
    -- right is recrusive quicksort of all elements greater than x.
    right = quicksort [x' | x' <- xs, x' >= x]

