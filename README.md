# snippets

Cool bash scripts, dotfiles, etc that others might find useful.

If you have anything you wish to add to this repo, clone it, create a new branch, and open a PR.

## Submission Rules

1. Follow UNIX philosophy: each file you submit should do or demonstrate one
   thing (or significantly related things.)
1. Use a sane naming scheme, appended with your unity ID (or first initial,
   last name if you don't have one.)
    * e.g. A backup script using tar might be named "tar-backup-wmharrel.sh"
1. Put things in the appropriate subfolder, by language (or the dotfiles
   folder.) If an appropriate folder doesn't yet exist, ask in Discord/IRC
   before creating one as part of your PR unless you're creating a folder for a
   specific language.
1. Add a header with at a minimum your unity ID, as many sentences necessary to
   explain what the purpose of the file is, and anything else the user would
   need to know.
1. Make a note of the file in this README as well, under submissions. Please
   alphabetize the list of files.
1. If you are adding a complicated piece of code or something that makes heavy
   use of piping, regex, or sed/awk, add comments. If whoever is reviewing your
   PR has to ask what a line does, you need more comments.

For an example, look at [bash/hello-world-wmharrel.sh][hello].

## Submissions

### Bash scripts (bash/)

* [brightness-dcclaibo.sh](bash/brightness-dcclaibo.sh): Command-line script
  for changing your primary screen's brightness.
* [hello-world-wmharrel.sh][hello]: Simple bash script to provide an example
  for this repo.

### Vim (vim/)

* [dcclaibo.vimrc](vim/dcclaibo.vimrc): Includes examples of `<LEADER>`, some
  misc. key maps, and a few visual settings that I enjoy.

### Haskell (haskell/)

* [simple-quicksort-wmharrel.hs](haskell/simple-quicksort-wmharrel.hs): Simple
  quicksort implementation in Haskell.

[hello]: bash/hello-world-wmharrel.sh
